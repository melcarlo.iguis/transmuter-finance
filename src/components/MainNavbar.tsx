import React, { useEffect, useState } from "react";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Button from "react-bootstrap/Button";
import logo from "../ico/TRANSMUTER_N (1).png";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import Avatar from "@mui/material/Avatar";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { useEthers } from "@usedapp/core";

function MainNavbar() {
  const { account, activateBrowserWallet, deactivate } = useEthers();

  const isConnected = account !== undefined;

  const setContent = (e: React.MouseEvent<HTMLElement>, data: string) => {
    localStorage.setItem("title", data);
  };
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );

  const settings = ["Profile", "Account", "Dashboard", "Logout"];

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <>
      <Navbar id="navbar" collapseOnSelect expand="lg">
        <Container>
          <Navbar.Brand href="/">
            <div className="btn">
              <img id="logo" src={logo} alt="Transmuter-logo" />
            </div>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse
            id="responsive-navbar-nav"
            style={{ textAlign: "right", width: "20%" }}
          >
            <Nav className="mx-auto">
              <Nav.Link
                className="link"
                href="/info/about"
                onClick={(e: React.MouseEvent<HTMLElement>) =>
                  setContent(e, "about")
                }
              >
                About
              </Nav.Link>
              <Nav.Link
                className="link"
                href="/info/community"
                onClick={(e: React.MouseEvent<HTMLElement>) =>
                  setContent(e, "community")
                }
              >
                Community
              </Nav.Link>
              <Nav.Link
                className="link"
                href="/info/docs"
                onClick={(e: React.MouseEvent<HTMLElement>) =>
                  setContent(e, "docs")
                }
              >
                Docs
              </Nav.Link>
              <Nav.Link
                className="link"
                href="/info/more"
                onClick={(e: React.MouseEvent<HTMLElement>) =>
                  setContent(e, "more")
                }
              >
                More
              </Nav.Link>

              {/* <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                <NavDropdown.Item href="">Price</NavDropdown.Item>
                <NavDropdown.Item href="">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  Something
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  Separated link
                </NavDropdown.Item>
              </NavDropdown> */}
            </Nav>

            <Nav.Link>
              <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar src="https://randomuser.me/api/portraits/men/34.jpg" />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  {settings.map((setting) => (
                    <MenuItem key={setting} onClick={handleCloseUserMenu}>
                      <Typography textAlign="center">{setting}</Typography>
                    </MenuItem>
                  ))}
                </Menu>
              </Box>
            </Nav.Link>
            <Nav>
              {isConnected ? (
                <Navbar.Brand href="">
                  <Navbar.Brand>
                    <Button className="button" onClick={deactivate}>
                      Disconnect
                    </Button>
                  </Navbar.Brand>
                </Navbar.Brand>
              ) : (
                <Navbar.Brand>
                  <Button className="button" onClick={activateBrowserWallet}>
                    Connect Wallet
                  </Button>
                </Navbar.Brand>
              )}
              {/* <Nav.Link href="">
                <Button className="button">Connect</Button>
              </Nav.Link> */}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default MainNavbar;

import React, { useEffect } from "react";
import AppNavbar from "../AppNavbar";
import { Container, Row, Col } from "react-bootstrap";
import Divider from "@mui/material/Divider";
import { Navigate, useNavigate } from "react-router-dom";
import transmuter from "../../ico/TRANSMUTER.png";
import fb from "../../ico/fb.png";
import twiiter from "../../ico/twitter.png";
import google from "../../ico/google.png";
import { auth, facebookProvider, googleProvider } from "../../Firebase";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  FacebookAuthProvider,
} from "firebase/auth";

import Swal from "sweetalert2";

function Signup() {
  let userData = JSON.parse(localStorage.getItem("userData") || "[]");
  const navigate = useNavigate();

  useEffect(() => {
    if (userData.length > 0) {
      // user already login
      navigate("/main");
    }
  }, [userData]);

  // function for google login
  const signinWithGoogle = () => {
    signInWithPopup(auth, googleProvider)
      .then((result) => {
        const userData = [
          {
            name: result.user.displayName,
            email: result.user.email,
            profilePic: result.user.photoURL,
          },
        ];
        localStorage.setItem("userData", JSON.stringify(userData));
        Swal.fire("Successfully login");
        navigate("/main");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const signinWithFacebook = () => {
    signInWithPopup(auth, facebookProvider)
      .then((result) => {
        console.log(result);
        const userData = [
          {
            name: result.user.displayName,
            email: result.user.email,
            profilePic: result.user.photoURL,
          },
        ];
        localStorage.setItem("userData", JSON.stringify(userData));
        Swal.fire("Successfully login");
        navigate("/main");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <AppNavbar />
      <div className="sign-up-main-holder position-relative mt-5">
        <div className="sign-up-holder ">
          <div className="img-holder pt-5">
            <img src={transmuter} alt="transmuter" />
          </div>
          <div className="form-holder">
            <form>
              <label>Email</label>
              <input type="email"></input>
              <label>Password</label>
              <input type="password"></input>
              <div className="btn-holder">
                <button>Sign up</button>
              </div>
            </form>
            <Divider>or Login with</Divider>
            <div className="d-flex justify-content-center mt-2 ">
              <img src={fb} alt="fb" onClick={signinWithFacebook} />
              <img src={google} alt="google" onClick={signinWithGoogle} />
              <img src={twiiter} alt="fb" />
            </div>
            <p className="d-block text-center mt-2">
              Already a User? <a href="/hehehhe">Login here</a>
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Signup;

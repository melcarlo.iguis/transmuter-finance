import React, { useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";
import logo from "../ico/transmuter-logo-light.png";
import { useEthers } from "@usedapp/core";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import { NavDropdown } from "react-bootstrap";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MailIcon from "@mui/icons-material/Mail";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MoreIcon from "@mui/icons-material/MoreVert";
import Avatar from "@mui/material/Avatar";
import Swal from "sweetalert2";

function CustomNavbar() {
  const navigate = useNavigate();
  const [navbarItemOpen, setNavbarItemOpen] = useState(false);
  const { account, activateBrowserWallet, deactivate } = useEthers();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const setContent = (e: React.MouseEvent<HTMLElement>, data: string) => {
    localStorage.setItem("title", data);
  };

  let userData = JSON.parse(localStorage.getItem("userData") || "[]");

  const isConnected = account !== undefined;
  const handleToggle = () => {
    setNavbarItemOpen((prevVal) => !prevVal);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });

  const logout = () => {
    localStorage.setItem("userData", "[]");
    Swal.fire("Successfully logout");
    navigate("/");
  };

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      keepMounted
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem>{userData.length > 0 ? userData[0].name : ""}</MenuItem>
      <MenuItem>{userData.length > 0 ? userData[0].email : ""}</MenuItem>
      <MenuItem onClick={logout}>Log out</MenuItem>
    </Menu>
  );

  return (
    <nav className="navbar">
      <div className="btn" onClick={() => navigate("/")}>
        <img id="logo" src={logo} alt="Transmuter-logo" />
      </div>
      <div onClick={() => handleToggle()} className="toggle-button">
        <span className="bar"></span>
        <span className="bar"></span>
        <span className="bar"></span>
      </div>
      <div
        className={
          navbarItemOpen && isTabletOrMobile
            ? "navbar-links-hide"
            : "navbar-links"
        }
      >
        <ul>
          <li>
            <a href="/main">Vault</a>
          </li>{" "}
          <li>
            <a
              href="/info/about"
              onClick={(e: React.MouseEvent<HTMLElement>) =>
                setContent(e, "about")
              }
            >
              About
            </a>
          </li>{" "}
          <li>
            <a
              href="/info/community"
              onClick={(e: React.MouseEvent<HTMLElement>) =>
                setContent(e, "community")
              }
            >
              Community
            </a>
          </li>
          <li>
            <a
              href="/info/docs"
              onClick={(e: React.MouseEvent<HTMLElement>) =>
                setContent(e, "docs")
              }
            >
              Docs
            </a>
          </li>
          <li></li>
        </ul>
      </div>
      <div
        className={
          navbarItemOpen && isTabletOrMobile
            ? "navbar-links-right-hide"
            : "navbar-links-right"
        }
      >
        <ul>
          <li>
            {" "}
            <IconButton
              size="small"
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
            >
              <Avatar src={userData.length > 0 ? userData[0].profilePic : ""} />
            </IconButton>
          </li>
          <li>
            {isConnected ? (
              <Button className="button mx-3" onClick={deactivate}>
                Disconnect
              </Button>
            ) : (
              <Button className="button mx-3" onClick={activateBrowserWallet}>
                Connect Wallet
              </Button>
            )}
          </li>
        </ul>
      </div>
      {renderMenu}
    </nav>
  );
}

export default CustomNavbar;

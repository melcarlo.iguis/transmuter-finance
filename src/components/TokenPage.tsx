import React, { useEffect, useState } from "react";
import CustomNavbar from "./CustomNavbar";
import { Container, Row, Col } from "react-bootstrap";
import { Button, Input, CircularProgress } from "@material-ui/core";
import Divider from "@mui/material/Divider";
import { Token } from "./Main";
import { useStakeTokens } from "../hooks";
import { BigNumber, constants, utils } from "ethers";
import { light } from "@mui/material/styles/createPalette";
import { useEthers, useTokenBalance } from "@usedapp/core";
import { formatUnits } from "@ethersproject/units";
import netWorkMapping from "../chain-info/deployments/map.json";

function TokenPage() {
  const tokenArray = JSON.parse(localStorage.getItem("tokenArray") || "{}");
  console.log(tokenArray);

  const [amount, setAmount] = useState<
    number | string | Array<number | string>
  >(0);

  const { chainId } = useEthers();
  const tokenFarmAddress = chainId
    ? netWorkMapping[String(chainId)]["TokenFarm"][0]
    : constants.AddressZero;

  const {
    approveAndStake,
    approveAndStakeErc20State,
    unStakeSendFunction,
    unstakeState,
  } = useStakeTokens(tokenArray.address);

  // const handleUnstakeSubmit = () => {
  //   unStakeSendFunction(tokenArray.address);
  // };

  const handleStakeSubmit = () => {
    proceedTransaction();
  };

  const proceedTransaction = () => {
    const amountAsWei = utils.parseEther(amount.toString());
    return approveAndStake(amountAsWei.toString());
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newAmount =
      event.target.value === "" ? "" : Number(event.target.value);
    setAmount(newAmount);
  };

  const isMining = approveAndStakeErc20State.status === "Mining";

  // token balance
  const GetTokenBalance = (tokenAddress: string) => {
    const { account } = useEthers();
    const tokenBalance = useTokenBalance(tokenAddress, account);

    console.log(tokenBalance);
    const formattedTokenBalance: number = tokenBalance
      ? parseFloat(formatUnits(tokenBalance, 18))
      : 0;

    return formattedTokenBalance;
  };

  const truncate = (str: string, n: number) => {
    return str?.length > n ? str.substr(0, n - 1) + "..." : str;
  };

  return (
    <>
      <div className="stake-main-container">
        <CustomNavbar />;
        <div className="token-holder">
          <Container fluid>
            <Row>
              <Col lg={4}>
                <div className="position-relative holder-main">
                  <div className="d-flex img-title-holder">
                    <p> {tokenArray.name}</p>
                    <img src={tokenArray.image} alt={tokenArray.image} />
                  </div>
                </div>
              </Col>

              <Col lg={3}>
                <div className="vertical-div"></div>
                <p className="mt-5">
                  Contract:{" "}
                  <a href="https://kovan.etherscan.io/address/0x83FcBf15d7037A2fDA21145CCB3543e3dc82c465">
                    {truncate(tokenFarmAddress, 10)}
                  </a>
                </p>
                <p className="mt-5">Your Deposit:</p>
                <p className="mt-5">
                  Balance: {GetTokenBalance(tokenArray.address)}
                </p>
              </Col>
              <Col lg={5}>
                <div className="vertical-div"> </div>
                <div className="d-flex mt-3">
                  <input
                    type="number"
                    className="input-stake"
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                      handleInputChange(event)
                    }
                  />
                  <Button
                    onClick={() => handleStakeSubmit()}
                    disabled={isMining}
                    className="d-block mx-auto btn stake-btn"
                  >
                    {isMining ? <CircularProgress size={26} /> : "Stake"}
                  </Button>
                  {/* <Button
                      onClick={() => handleUnstakeSubmit(index)}
                      disabled={isMining}
                      className="d-block mx-auto btn button"
                    >
                      {isMining ? <CircularProgress size={26} /> : "Unstake!"}
                    </Button> */}
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </>
  );
}

export default TokenPage;

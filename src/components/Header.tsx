import React from "react";
import Navbar from "react-bootstrap/Navbar";
import { Container, Row, Col } from "react-bootstrap";
import logo from "../logo.svg";
import { useEthers } from "@usedapp/core";

function Header() {
  const { account, activateBrowserWallet, deactivate } = useEthers();

  const isConnected = account !== undefined;

  return (
    <>
      <Navbar fixed="top" id="header">
        <Container>
          <Navbar.Brand className="company-name" href="/">
            <img
              alt=""
              src={logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />
            Transmuter finance
          </Navbar.Brand>
          {isConnected ? (
            <Navbar.Brand href="#home">
              <button className="btn-grad" color="primary" onClick={deactivate}>
                Disconnect
              </button>
            </Navbar.Brand>
          ) : (
            <Navbar.Brand>
              <button
                className="btn-grad"
                color="primary"
                onClick={activateBrowserWallet}
              >
                Connect Wallet
              </button>
            </Navbar.Brand>
          )}
        </Container>
      </Navbar>
    </>
  );
}

export default Header;

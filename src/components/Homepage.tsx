import React from "react";
import Header from "./Header";
import btc from "../ico/btc.png";
import et from "../ico/et.png";
import xrp from "../ico/xrp.png";
import ttr from "../ico/tether.png";
import Footer from "./Footer";
import bg from "../ico/bg.jpg";
import rocket from "../ico/rockett.png";
import usd from "../ico/usd.png";
import { Container, Row, Col } from "react-bootstrap";
import Cards from "./Cards";
import { useNavigate } from "react-router-dom";
import { AppBar } from "@mui/material";
import AppNavbar from "./AppNavbar";
import Frontpage from "./LandingPage/Frontpage";

function Homepage() {
  const navigate = useNavigate();

  return (
    <>
      <AppNavbar />
      <Frontpage />
    </>
  );
}

export default Homepage;

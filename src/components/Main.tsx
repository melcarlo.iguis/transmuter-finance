/* eslint-disable spaced-comment */
/// <reference types="react-scripts"/>
import React, { useState, useEffect } from "react";
import { Container } from "@material-ui/core";
import Header from "./Header";
import { useEthers } from "@usedapp/core";
import helperConfig from "../helper-config.json";
import networkMapping from "../chain-info/deployments/map.json";
import { constants } from "ethers";
import brownieConfig from "../brownie-config.json";
import dapp from "../ico/dapp.png";
import weth from "../ico/weth.png";
import dai from "../ico/dai.png";
import { YourWallet } from "./yourWallet";
import { ClassNames } from "@emotion/react";
import { ClassificationTypeNames } from "typescript";
import { makeStyles } from "@mui/material";
import { textAlign } from "@mui/system";
import AppNavbar from "./AppNavbar";
import MainNavbar from "./MainNavbar";
import StakingPage from "./StakingPage/StakingPage";
import CustomNavbar from "./CustomNavbar";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export type Token = {
  image: string;
  address: string;
  name: string;
};

// const useStyles = makeStyles((theme) => ({
//   title: {
//     color: theme.palette.common.white,
//     textAlign: "center",
//     padding: theme.spacing(4),
//   },
// }));
function Main() {
  // show token values from the wallet
  // get the address of the different tokens
  // get the balance of the user wallet
  // send the brownie-config to our 'src' folder
  // send the build folder
  const [isUserEmpty, setIsUserEmpty] = useState(true);

  // const classes = useStyles;
  const { chainId } = useEthers();
  console.log(chainId);
  const netWorkName = chainId ? helperConfig[chainId] : "dev";
  console.log(netWorkName);

  const dappTokenAddress = chainId
    ? networkMapping[String(chainId)]["DappToken"][0]
    : constants.AddressZero;

  const wethTokenAddress = chainId
    ? brownieConfig["networks"][netWorkName]["weth_token"]
    : constants.AddressZero;

  const fauTokenAddress = chainId
    ? brownieConfig["networks"][netWorkName]["fau_token"]
    : constants.AddressZero;

  const supportedTokens: Array<Token> = [
    {
      image: dapp,
      address: dappTokenAddress,
      name: "DAPP",
    },
    {
      image: weth,
      address: wethTokenAddress,
      name: "WETH",
    },
    {
      image: dai,
      address: fauTokenAddress,
      name: "DAI",
    },
  ];

  let userData = JSON.parse(localStorage.getItem("userData") || "[]");
  console.log(userData);
  const navigate = useNavigate();
  useEffect(() => {
    if (userData.length == 0) {
      Swal.fire({
        title: "Please login first",
        confirmButtonText: "Okay",
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          navigate("/signup");
        }
      });
    }
  }, [userData]);

  return (
    <>
      <div className="stake-main-container">
        <CustomNavbar />
        <div className="main-container mt-5">
          {/* <h2>Dapp Token App</h2> */}
          {/* <YourWallet supportedTokens={supportedTokens} /> */}
          <StakingPage supportedTokens={supportedTokens} />
        </div>
      </div>
    </>
  );
}

export default Main;

import React from "react";

interface InfoContentProps {
  title: string;
  body: string;
}

function InfoContent({ title, body }: InfoContentProps) {
  return (
    <div className="content-holder">
      <p className="title">{title}</p>
      <p className="body">{body}</p>
    </div>
  );
}

export default InfoContent;

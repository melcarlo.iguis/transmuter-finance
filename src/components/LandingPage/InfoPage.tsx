import React, { useEffect, useState } from "react";
import AppNavbar from "../AppNavbar";
import InfoContent from "./InfoContent";

function InfoPage() {
  let contentTitle = localStorage.getItem("title");
  const [index, setIndex] = useState(0);

  useEffect(() => {
    if (contentTitle == "about") {
      setIndex(0);
    } else if (contentTitle == "community") {
      setIndex(1);
    } else if (contentTitle == "docs") {
      setIndex(2);
    } else if (contentTitle == "more") {
      setIndex(3);
    }
  }, [contentTitle]);

  const infoContentArray = [
    {
      title: "About",
      body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus velit nisi placeat optio ad nesciunt? Corrupti earum iure repudiandae adipisci, cum porro illum maxime quisquam est voluptas? Obcaecati, perspiciatis repellendus.",
    },
    {
      title: "Community",
      body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus velit nisi placeat optio ad nesciunt? Corrupti earum iure repudiandae adipisci, cum porro illum maxime quisquam est voluptas? Obcaecati, perspiciatis repellendus.",
    },
    {
      title: "Docs",
      body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus velit nisi placeat optio ad nesciunt? Corrupti earum iure repudiandae adipisci, cum porro illum maxime quisquam est voluptas? Obcaecati, perspiciatis repellendus.",
    },
    {
      title: "More",
      body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus velit nisi placeat optio ad nesciunt? Corrupti earum iure repudiandae adipisci, cum porro illum maxime quisquam est voluptas? Obcaecati, perspiciatis repellendus.",
    },
  ];

  return (
    <>
      <AppNavbar />
      <InfoContent
        title={infoContentArray[index].title}
        body={infoContentArray[index].body}
      />
    </>
  );
}

export default InfoPage;

import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import btc from "../../ico/btc.png";
import lock from "../../ico/lock.png";
import scale from "../../ico/scale.png";
import link from "../../ico/chain.jpg";
import transmuter from "../../ico/TRANSMUTER.png";
import { FaArrowRight, FaBitcoin } from "react-icons/fa";
import sample from "../../ico/sample.gif";
import AppNavbar from "../AppNavbar";
import { useNavigate } from "react-router-dom";

function Frontpage() {
  const navigate = useNavigate();
  return (
    <>
      <AppNavbar />
      <Container fluid style={{ height: "100%" }}>
        <Row>
          <Col lg={6} md={12} className="div-container">
            <div className="main-contanier mt-5">
              <div className="btn d-flex">
                <div>
                  <FaBitcoin className="ico" style={{ fontSize: "20px" }} />
                </div>
                <p
                  className="px-1 link"
                  onClick={() => {
                    navigate("/main");
                  }}
                >
                  TRANSMUTER YOUR NEXT OPTION
                </p>

                <div>
                  <FaArrowRight className="ico" style={{ fontSize: "15px" }} />
                </div>
              </div>

              <div>
                <p className="header-one">Start your DeFi Options Journey</p>

                <p className="main-text">
                  Transmuter makes DeFi Options Easy and Accessible To Everyone.
                  Start Today!
                </p>
              </div>

              <div className="email-div mt-5">
                <label> Enter your email address*</label>

                <form className="d-flex email-input-holder">
                  <input
                    type="email"
                    placeholder="e.g., name@example.com"
                  ></input>
                  <button type="submit">Get Started</button>
                </form>
              </div>
              <Row className="mt-3 ico-holder">
                <Col md={4} sm={12}>
                  <Row>
                    <div className="d-flex icox">
                      <Col md={4} className="p-0 m-0 ">
                        <div className="img-holder p-0">
                          <img src={lock} alt="lock" />
                        </div>
                      </Col>
                      <Col md={8} className="p-0 m-0">
                        <p className="p-0">Lorem, ipsum dolor.</p>
                      </Col>
                    </div>
                  </Row>
                </Col>
                <Col md={4} sm={12}>
                  <Row>
                    <div className="d-flex icox">
                      <Col md={4} className="p-0 m-0">
                        <div className="img-holder">
                          <img src={scale} alt="scale" />
                        </div>
                      </Col>
                      <Col md={8} className="p-0 m-0">
                        <p>Flexibility & Scalability</p>
                      </Col>
                    </div>
                  </Row>
                </Col>
                <Col md={4} sm={12}>
                  <Row>
                    <div className="d-flex icox">
                      <Col md={4} className="p-0 m-0">
                        <div className="img-holder">
                          <img src={link} alt="link" />
                        </div>
                      </Col>
                      <Col md={8} className="p-0 m-0">
                        <p>Better Collaboration</p>
                      </Col>
                    </div>
                  </Row>
                </Col>
              </Row>
            </div>
          </Col>
          <Col lg={6} md={12} className="right-side-holder position-relative">
            <div className="right-side-child-container">
              <div className="image-holder">
                <img src={sample} alt="sample" />
                <div id="circle"></div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Frontpage;

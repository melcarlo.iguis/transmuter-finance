import React, { useEffect, useState } from "react";
import { Token } from "../Main";
import { Row, Col, Container } from "react-bootstrap";
import { Button, Input, CircularProgress, Snackbar } from "@material-ui/core";
import { useStakeTokens } from "../../hooks";
import { utils } from "ethers";
import { light } from "@mui/material/styles/createPalette";
import { useEthers, useTokenBalance } from "@usedapp/core";
import { formatUnits } from "@ethersproject/units";
import { useNavigate } from "react-router-dom";
import TokenPage from "../TokenPage";
import Alert from "@material-ui/lab/Alert";

interface YourWalletProps {
  supportedTokens: Array<Token>;
}

function StakingPage({ supportedTokens }: YourWalletProps) {
  const navigate = useNavigate();
  const [num, setNum] = useState(0);
  const [index, setIndex] = useState(0);
  const [unstakeIndex, setUnstakeIndex] = useState(0);

  const [amount, setAmount] = useState<
    number | string | Array<number | string>
  >(0);

  const {
    approveAndStake,
    approveAndStakeErc20State,
    unStakeSendFunction,
    unstakeState,
  } = useStakeTokens(supportedTokens[index].address);

  const handleUnstakeSubmit = (index: number) => {
    setUnstakeIndex(index);
    setIndex(index);
    unStakeSendFunction(supportedTokens[unstakeIndex].address);
  };

  const handleStakeSubmit = (index: number) => {
    console.log(index);
    proceedTransaction();
  };

  const proceedTransaction = () => {
    const amountAsWei = utils.parseEther(amount.toString());
    return approveAndStake(amountAsWei.toString());
  };

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newAmount =
      event.target.value === "" ? "" : Number(event.target.value);
    setAmount(newAmount);
    setIndex(index);
  };

  const isMining = approveAndStakeErc20State.status === "Mining";

  // token balance
  const GetTokenBalance = (tokenAddress: string) => {
    const { account } = useEthers();
    const tokenBalance = useTokenBalance(tokenAddress, account);

    console.log(tokenBalance);
    const formattedTokenBalance: number = tokenBalance
      ? parseFloat(formatUnits(tokenBalance, 18))
      : 0;

    return formattedTokenBalance;
  };

  const setToken = (data: any) => {
    localStorage.setItem("tokenArray", JSON.stringify(data));
  };

  return (
    <div>
      <Container fluid>
        <Row>
          {supportedTokens.map((token, index) => {
            return (
              <>
                <Col lg={4} md={12}>
                  <div
                    onClick={() => {
                      navigate(token.address);
                      setToken(token);
                    }}
                    key={index}
                    className="stake-token-holder"
                  >
                    <Col md={12}>
                      <div className="token-img-n-title mx-auto">
                        <img src={token.image} alt={token.image} />
                      </div>
                    </Col>
                    <Col md={12}>
                      <p className="token-name">{token.name}</p>
                    </Col>
                    {/* <Col md={4} sm={12}>
                      {" "}
                      <p className="number">{GetTokenBalance(token.address)}</p>
                    </Col> */}
                    <Col md={6} sm={12}>
                      {/* <div className="d-flex mt-3">
                        <input
                          type="number"
                          className="input-stake"
                          style={{ color: "black" }}
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ) => handleInputChange(event, index)}
                        />
                        <Button
                          onClick={() => handleStakeSubmit(index)}
                          disabled={isMining}
                          className="d-block mx-auto btn stake-btn"
                        >
                          {isMining ? <CircularProgress size={26} /> : "Stake!"}
                        </Button>
                        <Button
                          onClick={() => handleUnstakeSubmit(index)}
                          disabled={isMining}
                          className="d-block mx-auto btn button"
                        >
                          {isMining ? (
                            <CircularProgress size={26} />
                          ) : (
                            "Unstake!"
                          )}
                        </Button>
                      </div> */}
                    </Col>
                  </div>
                </Col>
              </>
            );
          })}
        </Row>
      </Container>
    </div>
  );
}

export default StakingPage;

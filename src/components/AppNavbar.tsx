import React, { useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";
import logo from "../ico/TRANSMUTER_N (1).png";
import { useEthers } from "@usedapp/core";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";

function CustomNavbar() {
  const navigate = useNavigate();
  const [navbarItemOpen, setNavbarItemOpen] = useState(false);
  const { account, activateBrowserWallet, deactivate } = useEthers();

  const setContent = (e: React.MouseEvent<HTMLElement>, data: string) => {
    localStorage.setItem("title", data);
  };
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );
  const isConnected = account !== undefined;
  const handleToggle = () => {
    setNavbarItemOpen((prevVal) => !prevVal);
  };

  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });

  return (
    <nav className="navbar-dark">
      <div className="btn" onClick={() => navigate("/")}>
        <img id="logo" src={logo} alt="Transmuter-logo" />
      </div>
      <div onClick={() => handleToggle()} className="toggle-button-dark">
        <span className="bar"></span>
        <span className="bar"></span>
        <span className="bar"></span>
      </div>
      <div
        className={
          navbarItemOpen && isTabletOrMobile
            ? "navbar-links-hide"
            : "navbar-links"
        }
      >
        <ul>
          <li>
            <a href="/main">Vault</a>
          </li>{" "}
          <li>
            <a
              href="/info/about"
              onClick={(e: React.MouseEvent<HTMLElement>) =>
                setContent(e, "about")
              }
            >
              About
            </a>
          </li>{" "}
          <li>
            <a
              href="/info/community"
              onClick={(e: React.MouseEvent<HTMLElement>) =>
                setContent(e, "community")
              }
            >
              Community
            </a>
          </li>
          <li>
            <a
              href="/info/docs"
              onClick={(e: React.MouseEvent<HTMLElement>) =>
                setContent(e, "docs")
              }
            >
              Docs
            </a>
          </li>
          <li>
            <a href="/about">Learn</a>
          </li>
        </ul>
      </div>
      <div
        className={
          navbarItemOpen && isTabletOrMobile
            ? "navbar-links-right-hide"
            : "navbar-links-right"
        }
      >
        <ul>
          <li>
            <a href="/signup">Join us</a>
          </li>{" "}
          <li>
            <a href="/about">Profile</a>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default CustomNavbar;

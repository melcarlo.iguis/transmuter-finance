// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  FacebookAuthProvider,
} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDlCNhB4525aClf1JSnL5ATOOUulPVDjMI",
  authDomain: "social-media-login-transmuter.firebaseapp.com",
  projectId: "social-media-login-transmuter",
  storageBucket: "social-media-login-transmuter.appspot.com",
  messagingSenderId: "843290535498",
  appId: "1:843290535498:web:b9d7e366ba79ccfbd868b3",
  measurementId: "G-9H2Z5EYQ0R",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const auth = getAuth(app);

export const googleProvider = new GoogleAuthProvider();
export const facebookProvider = new FacebookAuthProvider();

// export const signinWithGoogle = () => {
//   signInWithPopup(auth, googleProvider)
//     .then((result) => {
//       console.log(analytics);
//       const userData = [
//         {
//           name: result.user.displayName,
//           email: result.user.email,
//           profilePic: result.user.photoURL,
//         },
//       ];
//       localStorage.setItem("userData", JSON.stringify(userData));
//     })
//     .catch((error) => {
//       console.log(error);
//     });
// };

// export const signinWithFacebook = () => {
//   signInWithPopup(auth, facebookProvider)
//     .then((result) => {
//       console.log(result);
//       const name = result.user.displayName;
//       const email = result.user.email;
//       const profilePic = result.user.photoURL;
//       localStorage.setItem("name", name);
//       localStorage.setItem("email", email);
//       localStorage.setItem("profilePic", profilePic);
//     })
//     .catch((error) => {
//       console.log(error);
//     });
// };

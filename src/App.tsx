import React from "react";
import "./Styles/App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  Navigate,
  useParams,
} from "react-router-dom";
import Homepage from "./components/Homepage";
import { getDefaultProvider } from "ethers";
import {
  Mainnet,
  DAppProvider,
  useEtherBalance,
  useEthers,
  Config,
  ChainId,
  Kovan,
  Rinkeby,
} from "@usedapp/core";
import Header from "./components/Header";
import Main from "./components/Main";
import Signup from "./components/SignupPage/Signup";
import Frontpage from "./components/LandingPage/Frontpage";
import InfoPage from "./components/LandingPage/InfoPage";
import TokenPage from "./components/TokenPage";

const config: Config = {
  readOnlyChainId: Kovan.chainId,
  readOnlyUrls: {
    [Kovan.chainId]: getDefaultProvider("kovan"),
  },
};

function App() {
  return (
    <DAppProvider config={config}>
      <Router>
        <Routes>
          <Route path="/" element={<Frontpage />} />
          <Route path="/info/*" element={<InfoPage />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/main" element={<Main />} />
          <Route path="/main/:tokenAddress" element={<TokenPage />} />
        </Routes>
      </Router>
    </DAppProvider>
  );
}

export default App;

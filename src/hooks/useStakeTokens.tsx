import { useContractFunction, useEthers } from "@usedapp/core";
import { BigNumber, constants, utils } from "ethers";
import { Contract } from "@ethersproject/contracts";
import TokenFarm from "../chain-info/contracts/TokenFarm.json";
import ERC20 from "../chain-info/contracts/MockERC20.json";
import netWorkMapping from "../chain-info/deployments/map.json";
import { useState, useEffect } from "react";
import { isConstTypeReference } from "typescript";
import { useWeb3React } from "@web3-react/core";
import Web3 from "web3";
import { ethers } from "ethers";

export const useStakeTokens = (tokenAddress: string) => {
  // address
  //   abi
  //   chainId
  // web3 chain id
  // const { chainId } = useWeb3React();
  // console.log(chainId);

  const { chainId } = useEthers();
  const tokenFarmabi = TokenFarm.abi;
  const tokenFarmAddress = chainId
    ? netWorkMapping[String(chainId)]["TokenFarm"][0]
    : constants.AddressZero;

  const provider = window.ethereum;
  console.log(provider);
  // const signer = provider.getSigner();
  // console.log(signer);
  console.log(tokenFarmAddress);
  const tokenFarmInterface = new utils.Interface(tokenFarmabi);
  const tokenFarmContract = new Contract(tokenAddress, tokenFarmInterface);

  const erc20ABI = ERC20.abi;
  console.log(erc20ABI);
  const erc20Interface = new utils.Interface(erc20ABI);
  const er20Contract = new Contract(tokenAddress, erc20Interface);
  // const er20Contractweb3 = new Web3.eth.Contract(erc20ABI, tokenAddress);

  // approve
  const { send: approveEr20Send, state: approveAndStakeErc20State } =
    useContractFunction(er20Contract, "approve", {
      transactionName: "Approve ERC20 transfer",
    });

  const approveAndStake = (amount: string) => {
    setAmountToState(amount);
    return approveEr20Send(tokenFarmAddress, amount);
  };

  const { send: stakeSend, state: stakeState } = useContractFunction(
    tokenFarmContract,
    "stakeTokens",
    {
      transactionName: "Stake Token",
    }
  );

  const { send: unstakeSend, state: unstakeState } = useContractFunction(
    tokenFarmContract,
    "unstakeTokens",
    {
      transactionName: "Unstake Token",
    }
  );

  const unStakeSendFunction = (token: string) => {
    return unstakeSend(token);
  };

  const startStake = (amount: string) => {
    return stakeSend(amountToState, tokenAddress);
  };

  // unstaking function

  const [amountToState, setAmountToState] = useState("0");
  // useEffect
  useEffect(() => {
    if (approveAndStakeErc20State.status === "Success") {
      // stake function
      console.log("start staking");
      startStake(amountToState);
    }
  }, [approveAndStakeErc20State, tokenAddress, amountToState]);

  const [state, setState] = useState(approveAndStakeErc20State);

  useEffect(() => {
    if (approveAndStakeErc20State.status === "Success") {
      setState(stakeState);
    } else {
      setState(approveAndStakeErc20State);
    }
  }, [approveAndStakeErc20State, stakeState]);

  return {
    approveAndStake,
    approveAndStakeErc20State,
    unStakeSendFunction,
    unstakeState,
  };

  // stake token
};
